module.exports.tasks = [
  {
    id: "hometask-QWERy",
    type: "hometask",
    title: "QWERy",
    description: "Langing page to discover wonderful Indonesia with QWERy",
  },
  {
    id: "hometask-pet",
    type: "hometask",
    title: "Pet Adoption",
    description:
      "Langing page for a Pet Adoption Website (mobile and desktop layouts)",
  },
  {
    id: "classtask-YuliaVorman",
    type: "classtask",
    title: "YuliaVorman",
    description: "Langing page to advertice Yulia Vorman Beauty Masterclasses",
  },
  {
    id: "classtask-Humankind",
    type: "classtask",
    title: "Humantind",
    description: "A Humankind beauty landing page",
  },
  {
    id: "classtask-bloombeauty",
    type: "classtask",
    title: "Bloom Beauty",
    description: "A Page for Bloom Beauty cosmetics",
  },
  {
    id: "classtask-workshop-27-03",
    type: "classtask",
    title: "HR Workshop-27-03",
    description: "Workshop on March 27th for HR students (positions, z-index)",
  },
  {
    id: "classtask-workshop-30-03",
    type: "classtask",
    title: "HR Workshop-30-03",
    description: "Workshop on March 30th for HR students (images, sprites)",
  },
];
